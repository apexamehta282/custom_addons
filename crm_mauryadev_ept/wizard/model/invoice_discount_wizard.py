from openerp import fields,api,models,_ 
import openerp.addons.decimal_precision as dp
from openerp.osv import osv

class invoice_discount_wizard(models.TransientModel):
    
    _name='invoice.discount.wizard'
    
    discount= fields.Float('Discount (%)', digits_compute= dp.get_precision('Discount'))
    
    
    @api.multi    
    def apply_discount(self):
        account_invoice=self.env['account.invoice'].browse(self._context.get('active_ids',False))
        if len(account_invoice.invoice_line) < 1:
            raise osv.except_osv(_('Error!'),_('You cannot apply discount on invoice which has no line.'))
        else:
            if account_invoice.state=='draft':
                for line in account_invoice.invoice_line:
                    line.discount=self.discount
            else:
                raise osv.except_osv(_('Error!'),_("Discount is added only when invoice in draft state"))
            
    