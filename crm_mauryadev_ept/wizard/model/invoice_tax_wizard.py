from openerp import models,fields,api,_ 
from openerp.osv import osv

class invoice_tax_wizard(models.TransientModel):
    
    _name='invoice.account.tax.wizard'
    
    account_tax_code=fields.Many2many('account.tax', string="Tax Code")
    
    
    @api.multi
    def apply_tax(self):
        account_invoice=self.env['account.invoice'].browse(self._context.get('active_ids',False))
        if len(account_invoice.invoice_line) < 1:
            raise osv.except_osv(_('Error!'),_('You cannot apply tax on invoice which has no line.'))
        else:
            if account_invoice.state=='draft':
                for line in account_invoice.invoice_line:
                    line.invoice_line_tax_id=self.account_tax_code
            else:
                raise osv.except_osv(_('Error!'),_("Tax is added only when invoice in draft state"))