from openerp import fields,api,models,_ 
import openerp.addons.decimal_precision as dp
from openerp.osv import osv

class discount_wizard(models.TransientModel):
    
    _name='discount.wizard'
    
    discount= fields.Float('Discount (%)', digits_compute= dp.get_precision('Discount'))
    
    
    @api.multi    
    def apply_discount(self):
        sale_order=self.env['sale.order'].browse(self._context.get('active_ids',False))
        if len(sale_order.order_line) < 1:
            raise osv.except_osv(_('Error!'),_('You cannot apply discount on sales order which has no line.'))
        else:
            if sale_order.state=='draft':
                for line in sale_order.order_line:
                    line.discount=self.discount
            else:
                raise osv.except_osv(_('Error!'),_("Discount is added only when sale order in draft state"))
            
    