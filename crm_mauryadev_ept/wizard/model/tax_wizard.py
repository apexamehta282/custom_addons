from openerp import models,fields,api,_ 
from openerp.osv import osv

class tax_wizard(models.TransientModel):
    
    _name='account.tax.wizard'
    
    account_tax_code=fields.Many2many('account.tax', string="Tax Code")
    
    
    @api.multi
    def apply_tax(self):
        sale_order=self.env['sale.order'].browse(self._context.get('active_ids',False))
        if len(sale_order.order_line) < 1:
            raise osv.except_osv(_('Error!'),_('You cannot apply tax on sales order which has no line.'))
        else:
            if sale_order.state=='draft':
                for line in sale_order.order_line:
                    line.tax_id=self.account_tax_code
            else:
                raise osv.except_osv(_('Error!'),_("Tax is added only when sale order in draft state"))