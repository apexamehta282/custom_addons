# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'CRM Mauryadev Ept',
    'version': '1.0',
    'category': 'CRM',
    'sequence': 1,
    'summary': 'CRM Mauryadev Ept',
    'depends': ['mail',
                'delivery', 
                'crm',
                'sale_crm',],
    
    'data': [
             'security/crm_mauryadev_ept_security.xml',
             'security/ir.model.access.csv',
             'view/res_partner_view.xml',
             'view/crm_lead_view.xml',
             'view/account_invoice_view.xml',
             'view/stock_move_view.xml',
             'view/stock_picking_view.xml',
             'view/form_view_style_template.xml',
             'view/sale_view.xml',
             'view/res_partner_sequence.xml',
             'view/sale_order_sequence.xml',
             'wizard/view/discount_wizard_view.xml',
             'wizard/view/tax_wizard_view.xml',
             'wizard/view/invoice_discount_wizard_view.xml',
             'wizard/view/invoice_tax_wizard_view.xml',
             'data/ir_values_data.xml',
             'view/crm_phonecall_view.xml',
             'data/crm_case_stage_data.xml',
             'view/sale_line_view.xml',
             'view/account_invoice_line_view.xml',
             'report/sale_report.xml',
             'view/mail_style.xml',
             ],
    
    'installable': True,
    'auto_install': False,
    'qweb': [
        'static/src/xml/mail.xml',
    ],
}
