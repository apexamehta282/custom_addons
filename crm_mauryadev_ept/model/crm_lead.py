from openerp import models,fields,api,_
import random 
import string


class crm_lead(models.Model):
    
    _inherit='crm.lead'
    
    
    name=fields.Char('Opportunity Reference Number',required=False)
    email2=fields.Char(string='Email 2')
    email3=fields.Char(string='Email 3')
    phone2=fields.Char(string='Phone 2')
    phone3=fields.Char(string='Phone 3')
    port_id=fields.Many2one('res.partner.port',string="Port")
    customer_reference=fields.Char("Customer Reference")
    order_ids=fields.One2many('sale.order','lead_id',string="Order Lines")
    call_count=fields.Integer(compute='_call_count', string="Call Count")
    acount_invoice_ids=fields.One2many('account.invoice','lead_id',string="Invoice Lines")
    stock_picking_ids=fields.One2many('stock.picking','lead_id',string="Delivery Order Lines")
    
    @api.multi
    def on_change_partner_id(self,partner_id):
        values=super(crm_lead,self).on_change_partner_id(partner_id)
        res_partner_rec=self.env['res.partner'].browse([partner_id])
        vals={
            'email2':res_partner_rec.email2,
            'email3':res_partner_rec.email3,
            'phone2':res_partner_rec.phone2,
            'phone3':res_partner_rec.phone3,
            }
        values['value'].update(vals)
        return values
        
    @api.multi
    def _call_count(self): 
        for oppo_id in self.ids:
            crm_phonecall=self.env['crm.phonecall']
            self.call_count=crm_phonecall.search_count([('opportunity_id','=',oppo_id)])    
        
    @api.multi
    def opportunity_number_generation(self):
        return ''.join(random.choice(string.ascii_uppercase + string.digits) for i in range(4))                                                      
    
    @api.model
    def create(self, vals):
        if vals['type']=='opportunity':
            vals['name']='%s-%s'%(self.opportunity_number_generation(),self.opportunity_number_generation())                             
        return super(crm_lead,self).create(vals)
    
    @api.multi
    def _convert_opportunity_data(self, customer, section_id=False):
        val=super(crm_lead,self)._convert_opportunity_data(self._ids[0],customer,section_id=section_id)
        val['name']='%s-%s'%(self.opportunity_number_generation(),self.opportunity_number_generation())
        return val
    
    
    @api.multi
    def convert_to_customer(self):
        res_partner=self.env['res.partner']
        value={
            'name':"%s %s"%(self.title.name,self.contact_name),
            'type':'contact',
            'street':self.street,
            'city':self.city,
            'state_id':self.state_id.id,
            'country_id':self.country_id.id,
            'zip':self.zip,
            'email':self.email_from,
            'phone':self.phone,
            'user_id':self.user_id.id,
            }
        if self.partner_name:
            company_id=res_partner.search([('name','=',self.partner_name),('is_company','=',True)])
            if not company_id:
                vals={'name':self.partner_name,'type':'contact','is_company':True}
                company_id=res_partner.create(vals)
            value['parent_id']=company_id.id
        new_res_partner=res_partner.create(value)
        self.write({'partner_id':new_res_partner.id})
        self.message_post(body='<br><ul><li><b>Name: </b>%s</li><ul>'%(new_res_partner.display_name),subject='Partner Created')
        return True