from openerp import models,fields,api,_ 

class alternative_invoice_product_line(models.Model):
    
    _name='alternative.invoice.product.line'
    
    qty=fields.Float('Quantity')
    product_id=fields.Many2one('product.product',string="Product")
    description=fields.Text('Description')
    line_id=fields.Many2one('account.invoice.line',string='Account Invoice Line')
    
    @api.model
    def default_get(self, fields_list):
        res=super(alternative_invoice_product_line,self).default_get(fields_list)
        res['qty']=1.00
        return res
    
    @api.multi 
    def add_alternative_product(self):
        account_invoice_line=self.env['account.invoice.line']
        if self.line_id.sr_no:
            line_sr_no=account_invoice_line.search([('sr_no','ilike',self.line_id.sr_no[:1])],limit=1,order='id desc')
            if len(line_sr_no.sr_no) == 1:
                sr_no="%s%s"%(line_sr_no.sr_no,'A')
            elif line_sr_no.sr_no[-1]=='Z':
                sr_no="%s%s"%(line_sr_no.sr_no.replace('Z','A'),'A') 
            elif line_sr_no.sr_no >= 3 :
                sr_no="%s%s"%(line_sr_no.sr_no[:len(line_sr_no.sr_no)-1],chr(ord(line_sr_no.sr_no[-1])+1))
            else:
                sr_no="%s%s"%(line_sr_no.sr_no[:1],chr(ord(line_sr_no.sr_no[1:2])+1))
        else:
            sr_no=False
        uom_id=self.product_id.uom_id.id
        if self.description:
            name=self.description
        else: 
            name=False
        vals=account_invoice_line.product_id_change(self.product_id.id,uom_id,qty=self.qty, name=name,
                                                    type='out_invoice',
                                                    partner_id=self.line_id.invoice_id.partner_id.id, fposition_id=False, price_unit=self.line_id.price_unit, currency_id=self.line_id.invoice_id.currency_id.id,
                                                    company_id=self.line_id.invoice_id.company_id.id
                                                    )['value']
        vals['quantity']=self.qty
        vals['product_id']=self.product_id.id
        vals['invoice_id']=self.line_id.invoice_id.id
        vals['is_alt_product']=True
        if sr_no:
            vals['sr_no']=sr_no
        if self.description:
            vals['name']=self.description
            
        account_invoice_line.create(vals)
        return True