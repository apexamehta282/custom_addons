from openerp import models,fields,api,_ 

class crm_phonecall(models.Model):
    
    _inherit='crm.phonecall'
    
    @api.model
    def create(self, vals):
        new_id=super(crm_phonecall,self).create(vals)
        if new_id.opportunity_id:
            stage="stage_call_made"
            old_stage="stage_vessel_calling"
            ir_model_data=self.env['ir.model.data']
            try:
                stage_id=ir_model_data.get_object_reference('crm_mauryadev_ept',stage)[1]
                old_stage_id=ir_model_data.get_object_reference('crm_mauryadev_ept',old_stage)[1]
            except ValueError:
                stage_id=False
                old_stage_id=False
            if stage_id and old_stage_id and new_id.opportunity_id.stage_id.id==old_stage_id:
                new_id.opportunity_id.write({'stage_id':stage_id})
        return new_id
    
    