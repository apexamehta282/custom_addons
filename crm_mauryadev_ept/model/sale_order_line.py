from openerp import fields,models,api,workflow,_ 
import openerp.addons.decimal_precision as dp
from openerp.osv import osv


class sale_order_line(models.Model):
    _inherit = 'sale.order.line'
    
    sr_no = fields.Char("Sr No")
    
    price_subtotal =fields.Float(compute='_amount_line', string='Subtotal', digits_compute= dp.get_precision('Account')
                                )
    alternative_product_ids=fields.One2many('alternative.product.line','line_id',string='Alternative Product')
    is_alt_product=fields.Boolean('Is Alternative Product')
    count_alt_product=fields.Integer(compute='_count_alternative_product',string="Total Alternative Product",store=True)
   
    @api.depends('alternative_product_ids')
    def _count_alternative_product(self):
        for line in self[0].order_id.order_line:
            line.count_alt_product=len(line.alternative_product_ids)
    
    def _calc_line_base_price(self, cr, uid, line, context=None):
        return line.price_unit
    
    @api.multi
    def _amount_line(self):
        for line in self.browse(self.ids):
                price = self._calc_line_base_price(line)
                qty = self._calc_line_quantity(line)
                taxes = line.tax_id.compute_all(price,qty,
                                            line.product_id,
                                            line.order_id.partner_id)
                line.price_subtotal = line.order_id.pricelist_id.currency_id.round(taxes['total'])
    
    @api.multi
    def open_alt_product_form(self):
        view_id = self.env.ref('crm_mauryadev_ept.sale_order_line_form_ept').id
        context = self._context.copy()
        return{
            'name':'Alternative Products',
            'view_type':'form',
            'view_mode':'form',
            'views' : [(view_id,'form')],
            'res_model':'sale.order.line',
            'view_id':view_id,
            'type':'ir.actions.act_window',
            'target':'new',
            'flags': {'form': {'action_buttons': True}},
            'context':context, 
            'res_id':self.id,
            }
    
    