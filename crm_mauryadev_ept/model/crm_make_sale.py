from openerp import models,fields,api,_ 
from openerp.osv import osv



class crm_make_sale(models.TransientModel):
    
    _inherit='crm.make.sale'
    
    
    def makeOrder(self, cr, uid, ids, context=None):
        values=super(crm_make_sale,self).makeOrder(cr,uid,ids,context=context)
        sale_obj = self.pool.get('sale.order')
        case_obj = self.pool.get('crm.lead')
        case_stage_obj=self.pool.get('crm.case.stage')
        ir_model_data=self.pool.get('ir.model.data')
        stage='stage_producing_quote'
        try:
            stage_id=ir_model_data.get_object_reference(cr,uid,'crm_mauryadev_ept',stage)[1]
        except ValueError:
            stage_id=False
        sale_obj_id=sale_obj.browse(cr,uid,values['res_id'],context=context)
        data = context and context.get('active_ids', []) or []
        if sale_obj_id:
            for case in case_obj.browse(cr, uid, data, context=context):
                sale_obj_id.write({'lead_id':case.id,'email2':case.email2,'email3':case.email3,'phone2':case.phone2,
                                   'phone3':case.phone3,'port_id':case.port_id.id,'customer_reference':case.customer_reference})
                if stage_id:
                    case.write({'stage_id':stage_id})
        return values
        