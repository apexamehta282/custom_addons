from openerp import models,fields,api,_ 
import openerp.addons.decimal_precision as dp

class account_invoice(models.Model):
    
    _inherit='account.invoice'
    
    email2=fields.Char(string='Email 2', states={'draft':[('readonly',False)]} , readonly=True)
    email3=fields.Char(string='Email 3', states={'draft':[('readonly',False)]} , readonly=True)
    phone2=fields.Char(string='Phone 2', states={'draft':[('readonly',False)]} , readonly=True)
    phone3=fields.Char(string='Phone 3', states={'draft':[('readonly',False)]} , readonly=True)
    customer_po=fields.Char('Customer PO', states={'draft':[('readonly',False)]} , readonly=True)
    discount=fields.Float('Discount(%)',  digits_compute= dp.get_precision('Discount'), states={'draft':[('readonly',False)]} , readonly=True)
    currency=fields.Char(related="currency_id.name",string="currency")
    amount_discount=fields.Float('Amount Discount',digits_compute= dp.get_precision('Amount Discount') , compute="_compute_discount" )
    port_id=fields.Many2one('res.partner.port',string="Port" , states={'draft':[('readonly',False)]} , readonly=True)
    account_tax_code=fields.Many2many('account.tax',string="Tax Code" , states={'draft':[('readonly',False)]} , readonly=True)
    delivery_date=fields.Datetime(string="Delivery Date", states={'draft': [('readonly', False)]}, readonly=True)
    amount_untaxed = fields.Float(string='Subtotal', digits=dp.get_precision('Account'),
        store=True, readonly=True, compute='_compute_amount', track_visibility='always')
    lead_id=fields.Many2one('crm.lead',string='Lead')
    
    
    @api.depends('invoice_line.discount')
    def _compute_discount(self):
        for account_invoice in self:
            total_discount_amount=0
            for line in account_invoice.invoice_line:
                total_discount_amount=((line.price_unit*line.quantity)*line.discount)/100
            account_invoice.amount_discount=total_discount_amount
            
    @api.multi 
    def apply_discount(self):
        for line in self.invoice_line:
            line.discount=self.discount
            
    @api.multi        
    def apply_tax(self):
        for line in self.invoice_line:
            line.invoice_line_tax_id=self.account_tax_code
      
    @api.one
    @api.depends('invoice_line.price_subtotal', 'tax_line.amount')
    def _compute_amount(self):
        self.amount_untaxed = sum(line.price_subtotal for line in self.invoice_line)
        self.amount_tax = sum(line.amount for line in self.tax_line)
        self.amount_total = self.amount_untaxed + self.amount_tax     
            
    @api.multi
    def onchange_partner_id(self, type, partner_id, date_invoice=False,
            payment_term=False, partner_bank_id=False, company_id=False):
        result=super(account_invoice,self).onchange_partner_id(type, partner_id, date_invoice=date_invoice,
            payment_term=payment_term, partner_bank_id=partner_bank_id, company_id=company_id)
        res_partner_rec=self.env['res.partner'].browse([partner_id])
        vals={
            'email2':res_partner_rec.email2,
            'email3':res_partner_rec.email3,
            'phone2':res_partner_rec.phone2,
            'phone3':res_partner_rec.phone3,
            }
        result['value'].update(vals)
        return result
        