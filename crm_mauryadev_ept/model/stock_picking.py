from openerp import models,fields,api,_ 

class stock_picking(models.Model):
    
    _inherit='stock.picking'
    
    client_order_ref=fields.Char('Reference/Description')
    customer_po=fields.Char('Customer PO',states={'done': [('readonly', True)], 'cancel': [('readonly', True)]})
    comment=fields.Text('Comment',states={'done': [('readonly', True)], 'cancel': [('readonly', True)]})
    port_id=fields.Many2one('res.partner.port',string="Port",states={'done': [('readonly', True)], 'cancel': [('readonly', True)]})
    lead_id=fields.Many2one('crm.lead',string="Opportunity",readonly=True)
   
    def _get_invoice_vals(self, cr, uid, key, inv_type, journal_id, move, context=None):
        values=super(stock_picking,self)._get_invoice_vals(cr,uid,key,inv_type,journal_id,move,context=context)
        if move.picking_id.lead_id:
            values['lead_id']=move.picking_id.lead_id.id
        return values