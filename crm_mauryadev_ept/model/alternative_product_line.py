from openerp import models,fields,api,_ 


class alternative_product_line(models.Model):
    
    _name='alternative.product.line'
    
    qty=fields.Float('Quantity')
    product_id=fields.Many2one('product.product',string="Product")
    description=fields.Text('Description')
    line_id=fields.Many2one('sale.order.line',string='Sale Order Line')
    
    @api.model
    def default_get(self, fields_list):
        res=super(alternative_product_line,self).default_get(fields_list)
        res['qty']=1.00
        return res
    
    @api.multi 
    def add_alternative_product(self):
        sale_order_line=self.env['sale.order.line']
        if self.line_id.sr_no:
            line_sr_no=sale_order_line.search([('sr_no','ilike',self.line_id.sr_no[:1])],limit=1,order='id desc')
            if len(line_sr_no.sr_no) == 1:
                sr_no="%s%s"%(line_sr_no.sr_no,'A')
            elif line_sr_no.sr_no[-1]=='Z':
                sr_no="%s%s"%(line_sr_no.sr_no.replace('Z','A'),'A') 
            elif line_sr_no.sr_no >= 3 :
                sr_no="%s%s"%(line_sr_no.sr_no[:len(line_sr_no.sr_no)-1],chr(ord(line_sr_no.sr_no[-1])+1))
            else:
                sr_no="%s%s"%(line_sr_no.sr_no[:1],chr(ord(line_sr_no.sr_no[1:2])+1))
        else:
            sr_no=False
        vals={
            'product_uom_qty':self.qty,
            'product_id':self.product_id.id,
            'order_id':self.line_id.order_id.id,
            'is_alt_product':True
            }
        if sr_no:
            vals['sr_no']=sr_no
        if self.description:
            vals['name']=self.description
        sale_order_line.create(vals)
        
        self.unlink()

        return {
            'type': 'ir.actions.client',
            'tag': 'reload',
            }
        
    