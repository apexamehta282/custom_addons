from openerp import models,fields,api,_ 
import random

class res_partner(models.Model):
    
    _inherit='res.partner'
    
    is_ship=fields.Boolean('Is a Ship / OFFSHORE PLATFORM?',help="check if contact is a ship")
    imo=fields.Char('IMO')
    mmsi=fields.Char('MMSI')
    call_sign=fields.Char('Call Sign')
    flag=fields.Char('Flag')
    ais_type=fields.Char('AIS Type')
    gross_tonnage=fields.Integer('Gross Tonnage')
    deadweight=fields.Integer('Deadweight(T)')
    length=fields.Integer('Length(MTR)')
    breadth=fields.Integer('Breadth(MTR)')
    year_built=fields.Integer('Year Built')
    status_id=fields.Many2one('res.partner','Status')
    customer_reference=fields.Char("Customer Reference")
    ports=fields.Many2many('res.partner.port',string='ports')
    email2=fields.Char(string='Email 2')
    email3=fields.Char(string='Email 3')
    phone2=fields.Char(string='Phone 2')
    phone3=fields.Char(string='Phone 3')
    postal_address_street=fields.Char(string="Street")
    postal_address_street2=fields.Char(string="Street2")
    postal_address_city=fields.Char(string="City")
    postal_address_state_id=fields.Many2one('res.country.state',string="State")
    postal_address_zip=fields.Char(string='Zip')
    postal_address_country_id=fields.Many2one('res.country',string="Country")
    customer_number=fields.Char('Customer Id')
    
    @api.model
    def create(self, vals):
        if vals.get('is_company',False):
            company_name=vals['name'].replace(' ','')[0:4].upper()
            company=self.search([('customer_number','ilike',company_name),('is_company','=',True)],limit=1,order='id desc')
            if company:
                number=str(int(company.customer_number[4:])+1).zfill(4)
            else:
                number=str(1).zfill(4)
            vals['customer_number']="%s%s"%(company_name,number)
            
        if vals.get('is_ship',False):
            company_name=self.browse([vals['parent_id']]).name.replace(' ','')[0:4].upper()
            ship_unique_name="%s%s"%(company_name,vals['name'].replace(' ','')[0:2].upper())
            ship=self.search([('customer_number','ilike',ship_unique_name),('is_ship','=',True)],limit=1,order='id desc')
            if ship:
                number=str(int(ship.customer_number[6:])+1).zfill(2)
            else:
                number=str(1).zfill(2)
            vals['customer_number']="%s%s"%(ship_unique_name,number)
            
        if not vals.get('is_company',False) and not vals.get('is_ship',False):
            vals['customer_number']=self.env['ir.sequence'].next_by_code('customer.id.sequence')   
        new_customer_id = super(res_partner,self).create(vals)
        new_customer_id.message_post(body='<br><ul><li><b>Customer Id: </b>%s</ul></li>'%(new_customer_id.customer_number),
                                     subject='New Partner Created')
        return new_customer_id
    
    @api.multi
    def write(self, vals):
        old_customer_number=self.customer_number
        old_is_company=self.is_company
        old_is_ship=self.is_ship
        super(res_partner,self).write(vals)
        value={}
        if self.is_company:
            company_name=self.name.replace(' ', '')[0:4].upper()
            company=self.search([('customer_number','ilike',company_name),('is_company','=',True),('id','!=',self.id)],limit=1,order='id desc')
            if company:
                number=str(int(company.customer_number[4:])+1).zfill(4)
            else:
                number=str(1).zfill(4)
            value['customer_number']="%s%s"%(company_name,number)
        elif self.is_ship:
            ship_name=self.name[0:2].upper()
            company_name=self.browse([self.parent_id.id]).name.replace(' ', '')[0:4].upper()
            ship_unique_name="%s%s"%(company_name,ship_name)
            ship=self.search([('customer_number','ilike',ship_unique_name),('is_ship','=',True),('id','!=',self.id)],limit=1,order='id desc')
            if ship:
                number=str(int(ship.customer_number[6:])+1).zfill(2)
            else:
                number=str(1).zfill(2)
            value['customer_number']="%s%s"%(ship_unique_name,number)
            
        elif not self.is_company and not self.is_ship and (old_is_company!=self.is_company or old_is_ship!=self.is_ship):
            value['customer_number']=self.env['ir.sequence'].next_by_code('customer.id.sequence') 
   
        if value.get('customer_number',False):
            if(value['customer_number']!=old_customer_number):
                self.message_post(body="""<br><ul><li>
                                            Old Customer Id: </b>%s</li>
                                            <li>
                                            New Customer Id: </b>%s</li>
                                            </ul>"""%(self.customer_number,value['customer_number']),subject='New Customer Id Generated')   
        return super(res_partner,self).write(value)
   
    
    
    @api.multi
    def name_get(self):
        res=super(res_partner,self).name_get()
        for record in self.browse(self.ids):
            name=record.name
            if record.parent_id and not record.is_company:
                name="%s, %s"%(name,record.parent_name)
                res.append((record.id,name))
        return res
    