from openerp import models,fields,api,_
import openerp.addons.decimal_precision as dp
from openerp.osv import osv

class sale_order(models.Model):
    _inherit = 'sale.order'
    
    
    @api.depends('order_line.discount')
    def _compute_discount(self):  
        for sale_order in self:
            total_discount_amount=0
            for line in sale_order.order_line:
                total_discount_amount += ((line.price_unit*line.product_uom_qty)*line.discount)/100
            sale_order.amount_discount=total_discount_amount
      
    name=fields.Char('Order Reference', required=True, copy=False,
            readonly=True, states={'draft': [('readonly', False)], 'sent': [('readonly', False)]}, select=True , track_visibility="onchange")
    delivery_date=fields.Datetime(string="Delivery Date", states={'draft': [('readonly', False)],'sent': [('readonly', False)]}, readonly=True)
    quote_valid = fields.Datetime(string="Quote Valid Till", states={'draft': [('readonly', False)],'sent': [('readonly', False)]}, readonly=True)
    currency = fields.Char(related='currency_id.name',string="Currency")
    amount_discount = fields.Float('Discounts',compute="_compute_discount", digits_compute= dp.get_precision('Discount'), store=True)
    email2=fields.Char(string='Email 2', states={'draft': [('readonly', False)],'sent': [('readonly', False)]}, readonly=True)
    email3=fields.Char(string='Email 3', states={'draft': [('readonly', False)],'sent': [('readonly', False)]}, readonly=True)
    phone2=fields.Char(string='Phone 2', states={'draft': [('readonly', False)],'sent': [('readonly', False)]}, readonly=True)
    phone3=fields.Char(string='Phone 3', states={'draft': [('readonly', False)],'sent': [('readonly', False)]}, readonly=True)
    customer_po=fields.Char('Customer PO')
    comment=fields.Text('Comment', states={'draft': [('readonly', False)],'sent': [('readonly', False)]}, readonly=True)
    customer_reference=fields.Char("Customer Reference", states={'draft': [('readonly', False)],'sent': [('readonly', False)]}, readonly=True)
    port_id=fields.Many2one('res.partner.port',string="Port", states={'draft': [('readonly', False)],'sent': [('readonly', False)]}, readonly=True)
    amount_untaxed=fields.Float(compute="_amount_all_wrapper", 
                                digits_compute=dp.get_precision('Account'), string='Untaxed Amount',store=True,
                                multi='sums', help="The amount without tax.", track_visibility='always')
    amount_tax=fields.Float(compute="_amount_all_wrapper", digits_compute=dp.get_precision('Account'), string='Taxes',
            store=True,multi='sums', help="The tax amount.")
    amount_total=fields.Float(compute="_amount_all_wrapper", digits_compute=dp.get_precision('Account'), string='Total',
            store=True,
            multi='sums', help="The total amount.")
    lead_id=fields.Many2one('crm.lead','Leads')

    @api.multi
    def onchange_partner_id(self,partner_id):
        values=super(sale_order,self).onchange_partner_id(partner_id)
        res_partner_rec=self.env['res.partner'].browse([partner_id])
        vals={
            'email2':res_partner_rec.email2,
            'email3':res_partner_rec.email3,
            'phone2':res_partner_rec.phone2,
            'phone3':res_partner_rec.phone3,
            }
        values['value'].update(vals)
        return values
    
    def _prepare_invoice(self, cr, uid, order, lines, context=None):
        values=super(sale_order,self)._prepare_invoice(cr,uid,order,lines,context=context)
        vals={
                'email2':order.email2,
                'email3':order.email3,
                'phone2':order.phone2,
                'phone3':order.phone3,
                'port_id':order.port_id.id,
                'customer_po':order.customer_po,
                'lead_id':order.lead_id.id 
            }
        values.update(vals)
        return values


    def delivery_set(self, cr, uid, ids, context=None):
        line_ids=super(sale_order,self).delivery_set(cr,uid,ids,context=context)
        sale_order_line=self.pool.get('sale.order.line')
        for line in sale_order_line.browse(cr,uid,line_ids,context=context):
            line.write({'is_alt_product':True})
        return line_ids
            
    
    
    @api.multi 
    def action_wait(self):
        super(sale_order,self).action_wait()
        name=self.env['ir.sequence'].next_by_code('sale.order.seq')
        old_name=self.name
        self.write({'name':name})
        return True
    
    
    @api.one
    @api.depends('order_line','order_line.price_unit','order_line.tax_id','order_line.discount','order_line.product_uom_qty')
    def _amount_all_wrapper(self):
        """ Wrapper because of direct method passing as parameter for function fields """
        return self._amount_all()
    
    @api.multi
    def _amount_all(self):
        cur_obj = self.env['res.currency']
        res = {}
        for order in self:
            val = val1 = 0.0
            cur = order.pricelist_id.currency_id
            for line in order.order_line:
                val1 += line.price_subtotal
                val += self._amount_line_tax(line)
            order.amount_tax = cur.round(val)
            order.amount_untaxed = cur.round(val1)
            order.amount_total = order.amount_untaxed - order.amount_discount + order.amount_tax 
    
    