from openerp import fields,models,api,_ 
import openerp.addons.decimal_precision as dp


class account_invoice_line(models.Model):
    
    _inherit='account.invoice.line'
    
    sr_no=fields.Char(string="Sr No.")
    if_line_total_include=fields.Boolean(string='Is Computed In Total')
    price_subtotal = fields.Float(string='Amount', digits= dp.get_precision('Account'),
                                  store=True, readonly=True, compute='_compute_price')
    
    alternative_product_ids=fields.One2many('alternative.invoice.product.line','line_id',string='Alternative Product')
    is_alt_product=fields.Boolean('Is Alternative Product')
    
    
    
    @api.one
    @api.depends('price_unit', 'discount', 'invoice_line_tax_id', 'quantity',
        'product_id', 'invoice_id.partner_id', 'invoice_id.currency_id')
    def _compute_price(self):
        price = self.price_unit
        taxes = self.invoice_line_tax_id.compute_all(price, self.quantity, product=self.product_id, partner=self.invoice_id.partner_id)
        self.price_subtotal = taxes['total']
        if self.invoice_id:
            self.price_subtotal = self.invoice_id.currency_id.round(self.price_subtotal)
            
            
    @api.multi
    def open_alt_product_form(self):
        view_id = self.env.ref('crm_mauryadev_ept.account_invoice_line_form_ept').id
        context = self._context.copy()
        return{
            'name':'Alternative Products',
            'view_type':'form',
            'view_mode':'form',
            'views' : [(view_id,'form')],
            'res_model':'account.invoice.line',
            'view_id':view_id,
            'type':'ir.actions.act_window',
            'target':'new',
            'flags': {'form': {'action_buttons': True}},
            'context':context, 
            'res_id':self.id,
            }