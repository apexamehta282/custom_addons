from openerp import models,fields,api,_ 

class stock_move(models.Model):
    
    _inherit='stock.move'
    
    sr_no = fields.Char("Sr No",readonly="True",compute="_compute_sr_no")
    
    def _prepare_picking_assign(self, cr, uid, move, context=None):
        values=super(stock_move,self)._prepare_picking_assign(cr,uid,move,context=context)
        sale_order=self.pool.get('sale.order')
        sale_order_id=sale_order.search(cr,uid,[('name','=',move.origin)],context=context)
        if sale_order_id:
            sale_order_rec=sale_order.browse(cr,uid,sale_order_id,context=context)
            vals={
                 'client_order_ref':sale_order_rec.client_order_ref,
                 'customer_po':sale_order_rec.customer_po,
                 'port_id':sale_order_rec.port_id.id,
                 'lead_id':sale_order_rec.lead_id.id,
                 }
            values.update(vals)
        return values
    
        
    @api.multi
    def _compute_sr_no(self):
        move_line_no=1
        for line in self[0].picking_id.move_lines:
            line.sr_no=move_line_no
            move_line_no+=1
            